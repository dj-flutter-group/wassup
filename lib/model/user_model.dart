class User {
  int? id;
  String? username;
  String? lastName;
  String? email;
  String? password;
  Person? person;

  User(
      {this.id,
      this.username,
      this.lastName,
      this.email,
      this.password,
      this.person});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    lastName = json['last_name'];
    email = json['email'];
    password = json['password'];
    person =
        json['person'] != null ? new Person.fromJson(json['person']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['password'] = this.password;
    if (this.person != null) {
      data['person'] = this.person!.toJson();
    }
    return data;
  }
}

class Person {
  int? id;
  String? photo;
  String? created;
  String? updated;
  int? user;

  Person({this.id, this.photo, this.created, this.updated, this.user});

  Person.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    photo = json['photo'];
    created = json['created'];
    updated = json['updated'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['photo'] = this.photo;
    data['created'] = this.created;
    data['updated'] = this.updated;
    data['user'] = this.user;
    return data;
  }
}
