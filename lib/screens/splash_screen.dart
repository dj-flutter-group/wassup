import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wassup/config/config.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // @override
  // void initState() {
  //   Timer timer = new Timer(new Duration(seconds: 15), () {
  //     print('hello world');
  //     Navigator.pushReplacementNamed(context, '/home');
  //   });
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Configu.colors.primaryColor,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                Configu.assets.logo,
                fit: BoxFit.contain,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
