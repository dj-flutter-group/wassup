import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/model/ChatModel.dart';

class IndividualPage extends StatefulWidget {
  const IndividualPage({Key? key, required this.chatModel}) : super(key: key);
  final ChatModel chatModel;

  @override
  _IndividualPageState createState() => _IndividualPageState();
}

class _IndividualPageState extends State<IndividualPage> {
  bool show = false;
  FocusNode focusNode = FocusNode();
  TextEditingController _controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          show = false;
        });
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBarGlo(),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Configu.assets.bg), fit: BoxFit.cover),
        ),
        child: Stack(children: [
          ListView(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.all(3),
              color: Configu.colors.msgColor2,
              child: Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 55,
                    child: Card(
                      margin: EdgeInsets.only(left: 2, right: 2, bottom: 8),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: TextFormField(
                          focusNode: focusNode,
                          controller: _controller,
                          textAlignVertical: TextAlignVertical.center,
                          keyboardType: TextInputType.multiline,
                          maxLines: 5,
                          minLines: 1,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Type a message...",
                            hintStyle:
                                TextStyle(color: Configu.colors.msgColor1),
                            prefixIcon: IconButton(
                              icon: Icon(
                                Icons.emoji_emotions_outlined,
                              ),
                              onPressed: () {
                                focusNode.unfocus();
                                focusNode.canRequestFocus = false;
                                setState(() {
                                  show = !show;
                                });
                              },
                            ),
                            //contentPadding: EdgeInsets.all(5),
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 4, bottom: 8),
                    child: CircleAvatar(
                      backgroundColor: Configu.colors.primaryColor,
                      radius: 22,
                      child: IconButton(
                        icon: Icon(
                          Icons.send_outlined,
                          color: Configu.colors.msgColor2,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ]),
        /* onWillPop: () {
              if (show) {
                setState(() {
                  show = false;
                });
              } else {
                Navigator.pop(context);
              }
              return Future.value(false);
            }*/
      ),
    );
  }

  /*   Widget emojiSelect() {
    return EmojiPicker(
        config: Config(columns: 7),
        onEmojiSelected: (emoji, category) {
          
          print(emoji);
          setState(() {
            _controller.text = _controller.text + emoji.emoji;
          });
        });
  }*/

  Widget AppBarGlo() {
    return AppBar(
      backgroundColor: Configu.colors.msgColor2,
      leadingWidth: 70,
      titleSpacing: 0,
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 9),
          child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Icon(
              Icons.arrow_back,
              size: 20,
              color: Colors.black,
            ),
            Spacer(flex: 8),
            CircleAvatar(
              radius: 20,
              child: SvgPicture.asset(
                widget.chatModel.isGroup
                    ? "assets/images/groups.svg"
                    : "assets/images/person.svg",
                color: Colors.white,
                height: 36,
                width: 36,
              ),
              backgroundColor: Colors.blueGrey,
            ),
          ]),
        ),
      ),
      title: InkWell(
        onTap: (() {}),
        child: Container(
          margin: EdgeInsets.all(5),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.chatModel.name,
                  style: TextStyle(
                    fontSize: 18.5,
                    fontWeight: FontWeight.bold,
                    color: Configu.colors.textColor,
                  ),
                ),
                Text(
                  "last seen today at 12:05",
                  style: TextStyle(
                    fontSize: 13,
                    color: Configu.colors.primaryColor,
                  ),
                )
              ]),
        ),
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.search,
            size: 30,
          ),
          color: Configu.colors.textColor,
        )
      ],
      centerTitle: true,
    );
  }
}
