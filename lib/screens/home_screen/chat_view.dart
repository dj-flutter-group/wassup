import 'package:flutter/material.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/model/ChatModel.dart';
import 'package:wassup/screens/select_contact.dart';
import 'package:wassup/widgets/add_bouton.dart';
import 'package:wassup/widgets/customUI/custom_card.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<ChatModel> chats = [
    ChatModel(
        name: "INf417",
        icon: "groups.svg",
        time: "4:00",
        isGroup: true,
        currentMessage: "We have course today",
        status: "hey i'm using wassup"),
    ChatModel(
        name: "Adam's",
        icon: "assets/images/person.svg",
        time: "6:00",
        isGroup: false,
        currentMessage: "How bro",
        status: "hey i'm using wassup"),
    ChatModel(
        name: "Genie Logiciel SI",
        icon: "groups.svg",
        time: "4:00",
        isGroup: true,
        currentMessage: "We have course today",
        status: "hey i'm using wassup"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: AddButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (builder) => SelectContact()));
          },
        ),
        body: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Container(
                width: 600,
                //color: Colors.grey,
                decoration: BoxDecoration(color: Configu.colors.titleBar),
                padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                child: Text(
                  "Chats",
                  style: TextStyle(),
                )),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: chats.length,
                itemBuilder: (context, index) =>
                    CustomCard(chatModel: chats[index]),

                /*SingleChildScrollView(
                child: Column(
              children: [
                CustomCard(),
                CustomCard(),
                CustomCard(),
              ],
                  )),*/
              ),
            ),
          ],
        ));
  }
}
