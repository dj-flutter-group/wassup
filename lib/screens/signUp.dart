import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/screens/home_1.dart';
import 'package:wassup/screens/signIn.dart';
import 'package:wassup/state/api_state.dart';
import 'package:wassup/widgets/button.dart';

class SignUp extends StatefulWidget {
  static const rooteName = "signup-sreens";
  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  bool passToggle = true;
  final _formfield = GlobalKey<FormState>();

  // new _by TOM
  String _phonNumber = "";
  String _email = "";
  String _username = "";
  String _password = "";
  String _confPassword = "";
  bool _isNotLoading = true;

  void _registerNow() async {
    var isVaid = _formfield.currentState!.validate();
    if (!isVaid) {
      return;
    }
    _formfield.currentState!.save();

    // registration
    bool isRegister = await Provider.of<ApiState>(context, listen: false)
        .registerNow(_phonNumber, _email, _username, _confPassword);
    if (isRegister == false) {
      // Navigator.of(context).pushReplacementNamed(SignIn.rooteName);

      bool isLogin = await Provider.of<ApiState>(context, listen: false)
          .loginNow(_phonNumber, _password);

      if (!isLogin) {
        phoneNumberController.clear();
        emailController.clear();
        usernameController.clear();
        passwordController.clear();
        confirmPasswordController.clear();
        setState(() {
          _isNotLoading = isLogin;
        });
        Navigator.of(context).pushReplacementNamed(Conversation.rooteName);
      } else {
        Navigator.of(context).pushReplacementNamed(SignIn.rooteName);
      }
    } else {
      _showErrorAlert();
    }

    // login
  }

  @override
  Widget build(BuildContext context) {
    if (_isNotLoading == false) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    } else {
      return Scaffold(
        body: SingleChildScrollView(
          child: SafeArea(
              child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Form(
              key: _formfield,
              child: Column(
                children: [
                  const SizedBox(height: 50),
                  Image.asset(
                    Configu.assets.logo2,
                    fit: BoxFit.contain,
                  ),
                  const SizedBox(height: 50),
                  Text(
                    'Create your account',
                    style: TextStyle(
                        color: Configu.colors.textColor,
                        fontSize: 20,
                        fontWeight: FontWeight.w800),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    controller: phoneNumberController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Phone number",
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter your Phone number";
                      } else if (_isMobileNumberValid(
                              phoneNumberController.text) ==
                          false) {
                        return "Enter a valid number";
                      }
                    },
                    onSaved: (value) {
                      _phonNumber = value!;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: emailController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Email address",
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter your Email";
                      } else if (_isEmailValid(emailController.text) == false) {
                        return "Enter a valid number";
                      }
                    },
                    onSaved: (value) {
                      _email = value!;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.name,
                    controller: usernameController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Username",
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter your username";
                      }
                    },
                    onSaved: (value) {
                      _username = value!;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: passwordController,
                    obscureText: passToggle,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Password",
                      suffix: InkWell(
                        onTap: () {
                          setState(() {
                            passToggle = !passToggle;
                          });
                        },
                        child: Icon(passToggle
                            ? Icons.visibility
                            : Icons.visibility_off),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter Password";
                      } else if (passwordController.text.length < 8) {
                        return "Password Length should be more than 8 characters";
                      }
                    },
                    onChanged: (value) {
                      setState(() {
                        _password = value;
                      });
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: confirmPasswordController,
                    obscureText: passToggle,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Password confirmation",
                      suffix: InkWell(
                        onTap: () {
                          setState(() {
                            passToggle = !passToggle;
                          });
                        },
                        child: Icon(passToggle
                            ? Icons.visibility
                            : Icons.visibility_off),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Confirm your Password";
                      } else if (_password != value) {
                        return "The both Password are not the same !";
                      }
                    },
                    onSaved: (value) {
                      _confPassword = value!;
                    },
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  const SizedBox(height: 20),
                  buildForgotPassBtn(),
                  Button(
                    onPressed: () {
                      _registerNow();
                    },
                    buttonText: 'Sign up',
                    borderRadius: 6,
                    textColor: Configu.colors.msgColor2,
                  ),
                ],
              ),
            ),
          )),
        ),
        bottomNavigationBar: Container(
            alignment: Alignment.center,
            height: 50,
            color: Configu.colors.msgColor2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  ' Already have an account?',
                  style: TextStyle(color: Configu.colors.secondColor),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => SignIn()));
                  },
                  child: Text(
                    ' Login',
                    style: TextStyle(color: Configu.colors.primaryColor),
                  ),
                )
              ],
            )),
      );
    }
  }

  // functions
  bool _isEmailValid(String email) {
    String regexPattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    var regExp = RegExp(regexPattern);

    if (regExp.hasMatch(email)) {
      return true;
    }
    return false;
  }

  bool _isMobileNumberValid(String phoneNumber) {
    String regexPattern = r'^(?:[+0][1-9])?[0-9]{10,12}$';
    var regExp = RegExp(regexPattern);

    if (regExp.hasMatch(phoneNumber)) {
      return true;
    }
    return false;
  }

  Future<dynamic> _showErrorAlert() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          icon: Icon(
            Icons.warning_amber_rounded,
            size: 50,
            color: Configu.colors.primaryColor,
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 5),
          scrollable: true,
          content: Column(
            children: const [
              SizedBox(height: 10),
              ListTile(
                horizontalTitleGap: 1.0,
                iconColor: Colors.red,
                textColor: Colors.red,
                leading: Icon(Icons.person_add_disabled_outlined),
                subtitle: Text(
                  "A user with this phone number already exists !",
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: Configu.colors.primaryColor),
              child: const Text("OK"),
            )
          ],
        );
      },
    );
  }

  Widget buildForgotPassBtn() {
    return Container(
      child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          alignment: Alignment.center,
          height: 50,
          color: Configu.colors.msgColor2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AutoSizeText(
                style: TextStyle(
                  color: Configu.colors.textColor,
                ),
                'By signing up, you agree our',
                minFontSize: 6.0,
                maxLines: 2,
              ),
              InkWell(
                child: AutoSizeText(
                  ' terms of use',
                  style: TextStyle(
                    color: Configu.colors.primaryColor,
                  ),
                  minFontSize: 6.0,
                  maxLines: 2,
                ),
              )
            ],
          )),
    );
  }
}
