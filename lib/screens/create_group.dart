import 'package:flutter/material.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/model/ChatModel.dart';
import 'package:wassup/widgets/customUI/button_card.dart';
import 'package:wassup/widgets/customUI/contact_card.dart';

class CreateGroup extends StatefulWidget {
  const CreateGroup({Key? key}) : super(key: key);

  @override
  _CreateGroupState createState() => _CreateGroupState();
}

class _CreateGroupState extends State<CreateGroup> {
  Widget build(BuildContext context) {
    List<ChatModel> contacts = [
      ChatModel(
          name: "Marc",
          icon: "groups.svg",
          time: "4:00",
          isGroup: true,
          currentMessage: "We have course today",
          status: "hey i'm using wassup"),
      ChatModel(
          name: "Adam's",
          icon: "assets/images/person.svg",
          time: "6:00",
          isGroup: false,
          currentMessage: "How bro",
          status: "i will be rich"),
      ChatModel(
          name: "Tom",
          icon: "assets/images/person.svg",
          time: "4:00",
          isGroup: true,
          currentMessage: "We have course today",
          status: "i love code"),
      ChatModel(
          name: "Davila",
          icon: "groups.svg",
          time: "4:00",
          isGroup: true,
          currentMessage: "We have course today",
          status: "God bless me"),
      ChatModel(
          name: "Libert",
          icon: "assets/images/person.svg",
          time: "6:00",
          isGroup: false,
          currentMessage: "How bro",
          status: "python"),
      ChatModel(
          name: "Muriel",
          icon: "groups.svg",
          time: "4:00",
          isGroup: true,
          currentMessage: "We have course today",
          status: "Faith"),
    ];

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Configu.colors.secondColor, //change your color here
        ),
        backgroundColor: Configu.colors.msgColor2,
        title: Text(
          " Create Group",
          style: TextStyle(
              color: Configu.colors.secondColor, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.attachment,
                color: Configu.colors.secondColor,
              )),
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.search,
                  size: 25, color: Configu.colors.secondColor))
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ButtonCard(name: "New group", icon: Icons.group),
          Container(
              width: 600,
              //color: Colors.grey,
              decoration: BoxDecoration(color: Configu.colors.titleBar),
              padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
              child: Text(
                "Contacts on Wassup",
                style: TextStyle(),
              )),
          Expanded(
            child: ListView.builder(
                itemCount: contacts.length,
                itemBuilder: (context, index) =>
                    /*if (index == 0) {
                    return ButtonCard(name: "New group", icon: Icons.group);
                  } else
                    return */
                    ContactCard(
                      contact: contacts[index],
                    )),
          ),
        ],
      ),
    );
  }
}
