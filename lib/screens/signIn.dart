import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/state/api_state.dart';
import 'package:wassup/screens/home_1.dart';
import 'package:wassup/screens/signUp.dart';
import 'package:wassup/widgets/button.dart';

class SignIn extends StatefulWidget {
  static const rooteName = "login-sreens";
  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  final _formfield = GlobalKey<FormState>();
  bool passToggle = true;

  // new _by TOM
  String _phonNumber = "";
  String _password = "";
  bool _isNotLoading = true;

  void _loginNow() async {
    var isVaid = _formfield.currentState!.validate();
    if (!isVaid) {
      return;
    }
    _formfield.currentState!.save();
    bool isLogin = await Provider.of<ApiState>(context, listen: false)
        .loginNow(_phonNumber, _password);

    if (!isLogin) {
      phoneNumberController.clear();
      passwordController.clear();
      setState(() {
        _isNotLoading = isLogin;
      });
      Navigator.of(context).pushReplacementNamed(Conversation.rooteName);
    } else {
      _showErrorAlert();
    }
  }

  Future<dynamic> _showErrorAlert() {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          icon: Icon(
            Icons.warning_amber_rounded,
            size: 50,
            color: Configu.colors.primaryColor,
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 5),
          scrollable: true,
          content: Column(
            children: const [
              SizedBox(height: 10),
              ListTile(
                horizontalTitleGap: 1.0,
                iconColor: Colors.red,
                textColor: Colors.red,
                leading: Icon(Icons.person_off_outlined),
                subtitle: Text(
                  "Provided information do not match to a user, try again or Sing Up !",
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: Configu.colors.primaryColor),
              child: const Text("OK"),
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_isNotLoading == false) {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    } else {
      return Scaffold(
        body: SingleChildScrollView(
          child: SafeArea(
              child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Form(
              key: _formfield,
              child: Column(
                children: [
                  const SizedBox(height: 60),
                  Image.asset(
                    Configu.assets.logo2,
                    fit: BoxFit.contain,
                  ),
                  const SizedBox(height: 50),
                  Text(
                    'Retrieve your account',
                    style: TextStyle(
                        color: Configu.colors.textColor,
                        fontSize: 20,
                        fontWeight: FontWeight.w800),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    controller: phoneNumberController,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Phone number",
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter your Phone number";
                      }
                    },
                    // new 8by TOM
                    onSaved: (value) {
                      _phonNumber = value!;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    controller: passwordController,
                    obscureText: passToggle,
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(
                          color: Configu.colors.primaryColor,
                          width: 1.0,
                        ),
                      ),
                      labelText: "Password",
                      suffix: InkWell(
                        onTap: () {
                          setState(() {
                            passToggle = !passToggle;
                          });
                        },
                        child: Icon(passToggle
                            ? Icons.visibility
                            : Icons.visibility_off),
                      ),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Enter Password";
                      } else if (passwordController.text.length < 8) {
                        return "Password Length should be more than 8 characters";
                      }
                    },
                    // new _by TOM
                    onSaved: (value) {
                      _password = value!;
                    },
                  ),
                  const SizedBox(
                    height: 60,
                  ),
                  Button(
                    onPressed: () {
                      _loginNow();
                    },
                    buttonText: 'Sign In',
                    borderRadius: 6,
                    textColor: Configu.colors.msgColor2,
                  ),
                ],
              ),
            ),
          )),
        ),
        bottomNavigationBar: Container(
            alignment: Alignment.center,
            height: 50,
            color: Configu.colors.msgColor2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  ' Don\'t have an account?',
                  style: TextStyle(color: Configu.colors.secondColor),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .pushReplacementNamed(SignUp.rooteName);
                  },
                  child: Text(
                    ' Sign up',
                    style: TextStyle(color: Configu.colors.primaryColor),
                  ),
                )
              ],
            )),
      );
    }
  }
}
