import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as htpp;
import 'package:localstorage/localstorage.dart';
import 'package:wassup/model/user_model.dart';

class ApiState with ChangeNotifier {
  User? _currentUser;

  LocalStorage userStorage = LocalStorage("currentuser");
  LocalStorage storage = LocalStorage("usertoken");

  // String user_api = "http://michelbtompe.pythonanywhere.com";
  String user_api = "http://127.0.0.1:8000";

  // login service
  Future<bool> loginNow(String phoneNumer, String password) async {
    try {
      var url = Uri.parse("$user_api/login/");
      var response = await htpp.post(
        url,
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: json.encode(
          {
            'username': phoneNumer,
            'password': password,
          },
        ),
      );
      var data = json.decode(response.body) as Map;
      if (data.containsKey('token')) {
        // save the tokeng in localstorage
        storage.setItem('token', data['token']);
        print(storage.getItem("token"));

        // save the user in local storage
        saveConnectedUser();

        print("Login succesfully");

        return false;
      }
      return true;
    } catch (e) {
      print("Error to login");
      print(e);
      return true;
    }
  }

// register service
  Future<bool> registerNow(
      String phoneNumer, String email, String username, String password) async {
    try {
      var url = Uri.parse("$user_api/register/");
      var response = await htpp.post(
        url,
        headers: {
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: json.encode({
          'username': phoneNumer,
          'email': email,
          'last_name': username,
          'password': password,
        }),
      );
      var data = json.decode(response.body) as Map;
      if (data['error'] == false) {
        print("Register succesfully");
      }
      print(data);
      return data['error'];
    } catch (e) {
      print("Error to register");
      print(e);
      return true;
    }
  }

  // get the connected user
  Future<User?> getConnectedUser() async {
    try {
      var token = storage.getItem('token');
      var url = Uri.parse("$user_api/getUser/");
      var response = await htpp.get(
        url,
        headers: {"Authorization": "token $token"},
      );
      var data = json.decode(response.body);
      // print(data);
      _currentUser = User.fromJson(data);
      // print(_currentUser?.person?.photo);
      return _currentUser;
    } catch (e) {
      print("Error to get the connected user");
      print(e);
      return null;
    }
  }

  // save the conected user in localstorage
  void saveConnectedUser() async {
    try {
      var token = storage.getItem('token');
      var url = Uri.parse("$user_api/getUser/");
      var response = await htpp.get(
        url,
        headers: {"Authorization": "token $token"},
      );
      var data = json.decode(response.body);
      _currentUser = User.fromJson(data);

      userStorage.setItem("currentuser", json.encode(_currentUser));

      print("Welcome");
      print(_currentUser?.username);
    } catch (e) {
      print("Error to save the connected user");
      print(e);
    }
  }
}
