import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:wassup/config/functions.dart';
import 'package:wassup/model/user_model.dart';

class UserInfo extends StatefulWidget {
  const UserInfo({super.key});

  @override
  State<UserInfo> createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  LocalStorage userStorage = LocalStorage("currentuser");

  String? _myText = "Hello";

  void initgetSavedInfo() {
    // var data = userStorage.getItem('currentuser');
    Map<String, dynamic> data = json.decode(userStorage.getItem('currentuser'));
    User currentUser = User.fromJson(data);

    // if (data.isNotEmpty) {
    //   _myText = currentUser.username;
    // }

    print(currentUser.email);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _myText = "Hello TM";
    initgetSavedInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Info"),
        actions: [
          IconButton(
            onPressed: () {
              logoutNow(context);
            },
            icon: Icon(Icons.logout_rounded),
          ),
        ],
      ),
      body: Center(
        child: Text(_myText!),
      ),
    );
  }
}
