import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wassup/config/functions.dart';

class SplashWidget extends StatelessWidget {
  final int time;
  final Widget child, nextPage;

  const SplashWidget(
      {super.key,
      required this.time,
      required this.nextPage,
      required this.child});

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: time), () {
      route(context, nextPage, close: true);
    });
    return child;
  }
}
