import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wassup/config/config.dart';

class AddButton extends StatelessWidget {
  VoidCallback onPressed;

  AddButton({
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.transparent,
        child: FloatingActionButton(
          backgroundColor: Configu.colors.primaryColor,
          onPressed: onPressed,
          child: Icon(
            Icons.chat,
          ),
        ));
  }
}
