import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wassup/config/config.dart';
import 'package:wassup/model/ChatModel.dart';
import 'package:wassup/screens/individual_chat.dart';

class ButtonCard extends StatelessWidget {
  const ButtonCard({
    Key? key,
    required this.name,
    required this.icon,
  }) : super(key: key);

  final String name;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: InkWell(
        onTap: () {
          /* Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      IndividualPage(chatModel: this.chatModel)));*/
        },
        child: ListTile(
          leading: avatar(),
          title: Text(
            name,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  Widget avatar() {
    return CircleAvatar(
      radius: 20,
      child: Icon(
        icon,
        color: Configu.colors.msgColor2,
      ),
      backgroundColor: Configu.colors.primaryColor,
    );
  }
}
