import 'package:flutter/material.dart';
import 'package:wassup/config/config.dart';

class TextFormGlobal extends StatelessWidget {
  TextFormGlobal(
      {Key? key,
      required this.controller,
      required this.text,
      required this.textInputType,
      required this.borderColor,
      required this.obscure})
      : super(key: key);
  final TextEditingController controller;
  final String text;
  Color? borderColor;
  final TextInputType textInputType;
  final bool obscure;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      padding: const EdgeInsets.only(top: 3, left: 15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6),
        border: Border.all(
          color: borderColor ?? Configu.colors.secondColor,
        ),
        /*boxShadow: [
          BoxShadow(
            color: Config.colors.primaryColor,
            blurRadius: 7,
          ),
        ],*/
      ),
      child: TextFormField(
        controller: controller,
        keyboardType: textInputType,
        obscureText: obscure,
        decoration: InputDecoration(
            hintText: text,
            border: InputBorder.none,
            contentPadding: EdgeInsets.all(0),
            hintStyle: const TextStyle(
              height: 1,
            )),
      ),
    );
  }
}
