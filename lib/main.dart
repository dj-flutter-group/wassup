import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:wassup/model/user_model.dart';
import 'package:wassup/screens/home_1.dart';
import 'package:wassup/screens/signIn.dart';
import 'package:wassup/screens/signUp.dart';

import 'package:wassup/config/config.dart';
import 'package:wassup/screens/splash_screen.dart';
import 'package:wassup/state/api_state.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    LocalStorage userStorage = LocalStorage("currentuser");
    LocalStorage storage = LocalStorage("usertoken");
    return ChangeNotifierProvider(
      create: (context) => ApiState(),
      child: MaterialApp(
        title: "Wassup",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: "OpenSans",
          primaryColor: Configu.colors.primaryColor,
          primarySwatch: Colors.green,
        ),
        home: FutureBuilder(
          future: userStorage.ready,
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Scaffold(
                body: Center(
                  child: SplashScreen(),
                ),
              );
            }
            if (userStorage.getItem('currentuser') == null ||
                storage.getItem("token") == null) {
              return SignIn();
            }
            return Conversation();
          },
        ),
        routes: {
          // '/': (context) => SplashWidget(
          //       nextPage: SignIn(),
          //       time: 2,
          //       child: SplashScreen(),
          //     ),
          SignIn.rooteName: (context) => SignIn(),
          SignUp.rooteName: (context) => SignUp(),
          Conversation.rooteName: (context) => Conversation(),
        },
      ),
    );
  }
}
