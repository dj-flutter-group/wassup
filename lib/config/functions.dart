import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:wassup/screens/signIn.dart';

route(context, widget, {bool close = false}) => close
    ? Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => widget), (route) => false)
    : Navigator.of(context).push(MaterialPageRoute(builder: (_) => widget));

// logout
LocalStorage storage = LocalStorage("usertoken");
LocalStorage userStorage = LocalStorage("currentuser");

void logoutNow(BuildContext context) {
  storage.clear();
  userStorage.clear();
  print("Logout successfully");
  Navigator.of(context).pushReplacementNamed(SignIn.rooteName);
}
